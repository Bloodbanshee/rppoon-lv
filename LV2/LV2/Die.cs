﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2
{
    class Die
    {
        //1. Zadatak
        //private int numberOfSides;
        //private Random randomGenerator;
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}
        //public int Roll()
        //{
        //    return this.randomGenerator.Next(1, numberOfSides + 1);
        //}

        //2. Zadatak
        private int numberOfSides;
        private Random randomGenerator;
        public Die(int numberOfSides, Random randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }
        public int Roll()
        {
            return this.randomGenerator.Next(1, numberOfSides + 1);
        }
    }
}
