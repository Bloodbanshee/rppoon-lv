﻿using System;
using System.Collections.Generic;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. Zadatak
            //DiceRoller diceRoller = new DiceRoller();
            //const int numberOfDice = 20;

            //for(int i = 0; i < numberOfDice; i++)
            //{
            //    diceRoller.InsertDie(new Die(4)); 
            //}
            //diceRoller.RollAllDice();

            //IList<int> rollingResults = diceRoller.GetRollingResults();
            //foreach(int result in rollingResults)
            //{
            //    Console.WriteLine(result);
            //}

            //2.Zadatak
            DiceRoller diceRoller = new DiceRoller();
            Random randomGenerator = new Random();
            const int numberOfDice = 20;

            for (int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(4, randomGenerator));
            }
            diceRoller.RollAllDice();

            IList<int> rollingResults = diceRoller.GetRollingResults();
            foreach (int result in rollingResults)
            {
                Console.WriteLine(result);
            }
        }
    }
}
