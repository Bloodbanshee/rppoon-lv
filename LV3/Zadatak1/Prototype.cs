﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak1
{
    interface Prototype
    {
        Prototype Clone();
    }
}
