﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak2
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private RandomGenerator randomGenerator;

        private MatrixGenerator()
        {
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }

        public double[][] createMatrix(int a, int b)
        {
            double[][] matrix = new double[a][];

            for(int i = 0; i<a; i++)
            {
                matrix[i] = new double[b];
                for(int j=0;j<b;j++)
                {
                    matrix[i][j] = randomGenerator.NextDouble();
                }
            }
            return matrix;
        }
    }
}
