﻿using System;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            double[][] matrix = new double[3][];
            for(int i =0; i<3;i++)
            {
                matrix[i] = new double[3];
            }

            matrix = matrixGenerator.createMatrix(3, 3);
            
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    Console.Write(matrix[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
