﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak3
{
    class Logger
    {
        private static Logger instance;
        string filePath;

        private Logger()
        {
            this.filePath = @"C:\Users\danij\OneDrive\Radna površina\3.Zadatak.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(string note)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(note);
            }
        }
        public void setFilePath(string filePath)
        {
            this.filePath = filePath;
        }
    }
}
