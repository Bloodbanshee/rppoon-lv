﻿using System;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification consoleNotification = new ConsoleNotification("Danijel Blažanović", "Working title", "Big notification", DateTime.Now, Category.INFO, ConsoleColor.DarkMagenta);
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(consoleNotification);
        }
    }
}
