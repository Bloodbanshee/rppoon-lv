﻿using System;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification consoleNotification = new ConsoleNotification("Danijel Blažanović", "Working title", "Big notification", DateTime.Now, Category.INFO, ConsoleColor.DarkMagenta);
            NotificationManager notificationManager = new NotificationManager();
            NotificationBuilder notificationBuilder = new NotificationBuilder();

            notificationBuilder.SetAuthor("Marko").SetColor(ConsoleColor.DarkBlue).SetTitle("Title override");
            notificationManager.Display(consoleNotification);
            notificationManager.Display(notificationBuilder.Build());
        }
    }
}
