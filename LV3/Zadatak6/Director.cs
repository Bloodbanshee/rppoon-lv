﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak6
{
    class Director
    {
        public NotificationBuilder createAlertNotification(string Author)
            {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
        notificationBuilder.SetLevel(Category.ALERT).SetAuthor(Author);

            return notificationBuilder;
            }

        public NotificationBuilder createInfoNotification(string Author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetLevel(Category.INFO).SetAuthor(Author);

            return notificationBuilder;
        }

        public NotificationBuilder createErrorNotification(string Author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetLevel(Category.ERROR).SetAuthor(Author);

            return notificationBuilder;
        }
    }
}
