﻿using System;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            Director director = new Director();
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            NotificationManager notificationManager = new NotificationManager();

            notificationBuilder = director.createErrorNotification("Sven");
            notificationManager.Display(notificationBuilder.Build());
        }
    }
}
