﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak1
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> listData = dataset.GetData();
            int numberOfRows = listData.Count;
            int numberOfColumns = listData[0].Count;
            double[][] data = new double[numberOfRows][];

            for (int i = 0; i < numberOfRows; i++)
                data[i] = new double[numberOfColumns];

            for (int i = 0; i < numberOfRows; i++)
                for (int j = 0; j < numberOfColumns; j++)
                    data[i][j] = listData[i][j];
            return data;

        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
