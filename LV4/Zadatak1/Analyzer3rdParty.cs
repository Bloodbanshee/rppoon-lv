﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zadatak1
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int numberOfRows = data.Length;
            double[] results = new double[numberOfRows];
            for (int i = 0; i < numberOfRows; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int numberOfRows = data.Length;
            int numberOfColumns = data[0].Length;
            double[] results = new double[numberOfColumns];
            for (int i = 0; i < numberOfColumns; i++)
            {
                double[] column = new double[numberOfRows];
                for (int j = 0; j < numberOfRows; j++)
                {
                    column[j] = data[j][i];

                }
                results[i] = column.Average();
            }
            return results;

        }
    }
}
