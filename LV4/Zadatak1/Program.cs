﻿using System;
using System.Linq;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 2
            string filepath = @"C:\Users\danij\source\repos\RPPOON-LV\LV4\Zadatak1\LV4_test.txt";
            Dataset datasetToReadFile = new Dataset(filepath);
            IAnalytics analyzer = new Adapter(new Analyzer3rdParty());
            double[] averagePerColumn = analyzer.CalculateAveragePerColumn(datasetToReadFile);
            double[] averagePerRow = analyzer.CalculateAveragePerRow(datasetToReadFile);
            Console.WriteLine("Average per row:");
            foreach (double number in averagePerRow)
                Console.Write(number + " ");
            Console.WriteLine("");
            Console.WriteLine("Total average per row:" + averagePerRow.Average());
            Console.WriteLine("Average per column:");
            foreach (double number in averagePerColumn)
                Console.Write(number + " ");
            Console.WriteLine("");
            Console.WriteLine("Total average per column: " + averagePerColumn.Average());

        }
    }
}
