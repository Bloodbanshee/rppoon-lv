﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak1
{
    interface IShippable
    {
        double Price { get; }
        double Weight { get; }
        string Description(int depth = 0);
    }
}
