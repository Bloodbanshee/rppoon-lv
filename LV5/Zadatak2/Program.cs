﻿using System;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product Resource = new Product("Vespene Gas", 150, 5);
            Product Crystal = new Product("Minerals", 300, 5);

            Box SupplyDrop = new Box("Boxed Goods");
            SupplyDrop.Add(Resource);
            SupplyDrop.Add(Crystal);

            ShippingService shippingService = new ShippingService(5);
            Console.WriteLine(shippingService.getDeliveryCost(SupplyDrop));
            Console.WriteLine(shippingService.getDeliveryCost(Resource));
        }
    }
}
