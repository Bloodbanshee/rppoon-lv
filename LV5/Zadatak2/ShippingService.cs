﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak2
{
    class ShippingService
    {
        private double pricePerKg;

        public ShippingService(double pricePerKg)
        {
            this.pricePerKg = pricePerKg;
        }

        public double getDeliveryCost(IShippable shippable)
        {
            return shippable.Weight * this.pricePerKg;
        }
    }
}
