﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak3
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }

        public void printData(IDataset dataset)
        {
            if (dataset.GetData() != null)
            {
                Console.WriteLine();
            foreach (List<string> list in dataset.GetData())
            {
                foreach (String number in list)
                {
                    Console.Write(number);
                }
                Console.WriteLine();
            }
            }
        }
    }
}
