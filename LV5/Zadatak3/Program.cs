﻿using System;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            VirtualProxyDataset virtualProxy = new VirtualProxyDataset(@"C:\Users\danij\OneDrive\Radna površina\LV5.txt");
            User user1 = User.GenerateUser("Danijel");
            User user2 = User.GenerateUser("Leinad");

            ProtectionProxyDataset protectionProxy = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset protectionProxy1 = new ProtectionProxyDataset(user2);

            DataConsolePrinter consolePrinter = new DataConsolePrinter();

            consolePrinter.printData(virtualProxy);
            consolePrinter.printData(protectionProxy);
            consolePrinter.printData(protectionProxy1);

        }
    }
}
