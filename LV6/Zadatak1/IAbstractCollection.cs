﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak1
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
