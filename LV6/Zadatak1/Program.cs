﻿using System;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Building", "Get the artifact to upgrade temple"));
            notebook.AddNote(new Note("Population", "Build a Hovel to increase pop cap"));
            notebook.AddNote(new Note("Alert", "Enemies sighted on the map"));
            notebook.AddNote(new Note("Goal", "Build the wonder by the end of next year"));
            Iterator iterator = new Iterator(notebook);

            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
           
        }
    }
}
