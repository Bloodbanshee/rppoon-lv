﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak2
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
