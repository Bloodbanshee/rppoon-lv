﻿using System;
using System.Collections.Generic;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product Resource1 = new Product("Wood", 150);
            Product Resource2 = new Product("Ore", 300);
            Product Resource3 = new Product("Population", 20);
            List<Product> products = new List<Product>() { Resource1, Resource2, Resource3 };
            Box box = new Box(products);


            printProducts(box);

            box.RemoveProduct(Resource1);
            printProducts(box);

            void printProducts(Box box)
            {
                Iterator iterator = (Iterator)box.GetIterator();
                for (int i = 0; i < box.Count; i++)
                {
                    Console.Write(iterator.Current.ToString());
                    iterator.Next();
                    Console.WriteLine("\n");
                }
            }
        }
    }
}
