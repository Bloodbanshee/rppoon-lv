﻿using System;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numberSequence = new double[] { -8, -44, 5, -63, 88, 15, 44, 97 };
            BubbleSort bubbleSort = new BubbleSort();
            CombSort combSort = new CombSort();
            SequentialSort sequentialSort = new SequentialSort();

            NumberSequence bubbleSequence = new NumberSequence(numberSequence);
            NumberSequence combSequence = new NumberSequence(numberSequence);
            NumberSequence sequentialSequence = new NumberSequence(numberSequence);

            bubbleSequence.SetSortStrategy(bubbleSort);
            combSequence.SetSortStrategy(combSort);
            sequentialSequence.SetSortStrategy(sequentialSort);

            System.Console.WriteLine("Bubble sort before : \n" + bubbleSequence.ToString());
            bubbleSequence.Sort();
            System.Console.WriteLine("Bubble sort after : \n" + bubbleSequence.ToString());
            System.Console.WriteLine("---------------------------");
            System.Console.WriteLine("Comb sort before : \n" + combSequence.ToString());
            combSequence.Sort();
            System.Console.WriteLine("Comb sort after : \n" + combSequence.ToString());
            System.Console.WriteLine("---------------------------");
            System.Console.WriteLine("Sequential sort before : \n" + sequentialSequence.ToString());
            sequentialSequence.Sort();
            System.Console.WriteLine("Sequential sort after : \n" + sequentialSequence.ToString());



        }
    }
}
