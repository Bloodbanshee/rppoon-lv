﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak2
{
    class LinearSearch : SearchStrategy
    {
        public override void Search(double[] array, double searchNumber)
        {
            bool founded = false;
            int arraySize = array.Length;
            int i = 0;
            for (i = 0; i < arraySize; i++)
            {
                if (searchNumber == array[i])
                {
                    founded = true;
                    break;
                }
            }
            if (founded)
            {
                Console.WriteLine("Number found on index number " + i.ToString());
            }
            else
            {
                Console.WriteLine("No such number found");
            }
        }

    }
}
