﻿using System;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numberSequence = new double[] { -8, -44, 5, -63, 88, 15, 44, 97 };
            LinearSearch linearSearch = new LinearSearch();
            NumberSequence searchSequence = new NumberSequence(numberSequence);

            searchSequence.SetSearchStrategy(linearSearch);
            searchSequence.SetSearchNumber(5);
            searchSequence.Search();
        }
    }
}
