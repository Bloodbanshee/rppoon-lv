﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak2
{
    abstract class SearchStrategy
    {
        public abstract void Search(double[] array, double number);
    }
}
