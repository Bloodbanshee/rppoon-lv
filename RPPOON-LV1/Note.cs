﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV1
{
    class Note
    {
        private String text;
        private String author;
        private int priority;

        public override string ToString() { return this.text + ", Author:" + author; }

        public string getText() {return this.text;}
        public void setText(string text) { this.text = text; }

        public string getAuthor() { return this.author; }

        public int getPriority() { return this.priority; }
        public void setPriority(int priority) { this.priority = priority; }

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }

        public int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }

        public Note(string mText, string mAuthor, int mPriority)
        {
            this.text = mText;
            this.author = mAuthor;
            if (mPriority >= 1 && mPriority <= 2)
            {
                this.priority = mPriority;
            }
            else Console.WriteLine("Error: Priority can only be set as 1 or 2.");
        }

        public Note(string mText, int mPriority)
        {
            this.text = mText;
            this.author = "Danijel Blazanovic";
            this.priority = mPriority;
        }

        public Note()
        {
            this.text = "Enter text";
            this.author = "Danijel Blazanovic";
            this.priority = 2;
        }
    }
}
