﻿using System;

namespace RPPOON_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note01 = new Note();
            Note note02 = new Note("Buy Bread","Danijel Blazanovic",1);
            Note note03 = new Note("Buy Juice",2);
            Console.WriteLine(note01.Text);
            Console.WriteLine(note01.Text= "Buy Ham");
            Console.WriteLine(note01.Author);
            Console.WriteLine(note02.Text);
            Console.WriteLine(note02.Author);
            Console.WriteLine(note03.Text);
            Console.WriteLine(note03.Author);
            Console.WriteLine(note01.ToString());
            Console.WriteLine(note02.ToString());
            Console.WriteLine(note03.ToString());
        }
    }
}
